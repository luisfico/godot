# comentario en GDScript language
extends Area2D   #declaro la variable position?

#var Velocidad = 400    #se define en el script
export (int) var Velocidad  #se define en el inspector
#la variable se reflejara en el inspector grafico (ir al nodo, inspector, en Scrip Variables)

var Movimiento = Vector2() #default x,y 0,0    delta de posicion
var limite
signal golpe # SEÑAL de COLISION: las señales son como eventos, si hay la señal luego se puede ejecutar una accion

# es la funcion para un evento de un nodo
func _ready():
	hide()  #que al inicio se oculte
	limite = get_viewport_rect().size #toma el tamaño de la pantalla
	# Initialization here
	#pass

#Define la frecuencia del juego: FPS (FramePerSecond)
func _process(delta): #velocidad de la PC
	Movimiento = Vector2(); #deltaX se reinicia  a 0 para que en cada bucle verifique si la la tecla sigue presiona 

	#Si presionas una tecla (ejm la flecha derecha)   (ejes XY en la esquina superior iquierda)
	if Input.is_action_pressed("ui_right"):
		Movimiento.x += 1
	if Input.is_action_pressed("ui_left"):
		Movimiento.x -= 1
	if Input.is_action_pressed("ui_down"):
		Movimiento.y += 1
	if Input.is_action_pressed("ui_up"):
		Movimiento.y -= 1
	#para que no se mueva más rapido en diagonal
	if Movimiento.length() > 0 : # verificar si se esta moviendo
		Movimiento = Movimiento.normalized() * Velocidad
		
	#ACTUALIZAR LOS MOVIMIENTOS: Para que  El juego  simepre sea CONSISTENTE y siempre lleve la misma velocidad , asi no depende de la velocidad de la PC 
	position += Movimiento * delta     # X = deltaX * deltaT ???    quien declaro position?

	#hace que el personaje no se salga de la pantalla  CLAMP-Restringe
	position.x = clamp(position.x, 0, limite.x) #asigna a la posX el valor del limite
	position.y = clamp(position.y, 0, limite.y)
	
	if Movimiento.x != 0: #si esta moviendo en X
		$Sprite_player.animation = "lado"  # el simbolo "$" busca algo en la ESCENA, en el sprite, y luego busca en el parametro animation  y lo setea en "lado" 
		$Sprite_player.flip_h = Movimiento.x < 0 #invierte el sprite cuando se mueve a la izq   
		#dar la vuelta / invertir cuando se mueve a la izq      osea cuando (Movimiento.x < 0) = true=1
		#$Sprite_player.flip_h = true #invierte si es true . Es true cuando el movimienzo es - osea a la izq
		$Sprite_player.flip_v =  0  #false  -> cuando el jugador se mueve de lado no invierte verticalmente el sprit
	elif Movimiento.y != 0:
		$Sprite_player.animation = "espalda"
		$Sprite_player.flip_v = Movimiento.y > 0  #invierte el sprite cuando se mueve hacia abajo  
		#$Sprite_player.flip_v =  0 
	else: #si no se esta moviendo se queda de frente el sprite   osea cuando (Movimiento.y > 0) = true=1
		$Sprite_player.animation = "frente"
		

#Este codigo fue generado automaticamente
#Se agrego el evento de _on_Player_body_entered del sprite -->ver min3 en video: https://www.youtube.com/watch?v=J5iI3SLdNAE&list=PL5K_XeigIfdJeaJ3-_YgnzD711StmJkZh&index=4     
func _on_Player_body_entered(body):  #evento de COLISION: cuando UN CUERPO toca a otro CUERPO
	#pass # replace with function body       ¿que quieres que pase cuando se de este evento?
	hide()  #el personaje se oculta cunado COLISIONA con otro
	emit_signal("golpe") #emite la señal golpe
	$CollisionShape2D.disabled = true    #desactivamos el area de colisiom (osea el obtjeo CollisionShape2D)
